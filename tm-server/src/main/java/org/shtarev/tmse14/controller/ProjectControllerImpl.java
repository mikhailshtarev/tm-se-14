package org.shtarev.tmse14.controller;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse14.entity.Project;
import org.shtarev.tmse14.service.ProjectService;

import javax.jws.WebService;

@WebService(endpointInterface = "org.shtarev.tmse14.controller.ProjectController")
public class ProjectControllerImpl implements ProjectController {


    private final @NotNull ProjectService<Project> projectService;

    public ProjectControllerImpl(final @NotNull ProjectService<Project> projectService) {
        this.projectService = projectService;
    }
//
//    @Override
//    public boolean projectCreate(@NotNull final Project project, @NotNull final Sesssion sesssion) {
//        return projectService.create(project, sesssion);
//    }
//
//    @Override
//    public List<Project> projectsReadAll(@NotNull final Sesssion sesssion) throws IOException {
//        Optional<List<Project>> listProject = projectService.projectsReadAll(sesssion);
//        if (listProject.isPresent())
//            return listProject.get();
//        else throw new IOException();
//    }
//
//    @Override
//    public List<Project> projectFindByDescription(@NotNull final String partName, @NotNull final Sesssion sesssion) throws IOException {
//        Optional<List<Project>> listProject = projectService.findByDescription(partName, sesssion);
//        if (listProject.isPresent())
//            return listProject.get();
//        else throw new IOException();
//    }
//
//    @Override
//    public List<Project> projectSorted(@NotNull final String sort, @NotNull final Sesssion sesssion) throws IOException {
//        Optional<List<Project>> listProject = projectService.sorted(sort, sesssion);
//        if (listProject.isPresent())
//            return listProject.get();
//        else throw new IOException();
//    }
//
//    @Override
//    public boolean projectUpdate(@NotNull final String projectId, @NotNull final Project project,
//                                 @NotNull final String taskProjectStatus, @NotNull final Sesssion sesssion) {
//        try {
//            return projectService.update(projectId, project, taskProjectStatus, sesssion);
//        } catch (IOException e) {
//            e.printStackTrace();
//            return false;
//        }
//
//    }
//
//    @Override
//    public boolean projectDelete(@NotNull final String projectId, @NotNull final Sesssion sesssion) {
//        try {
//            return projectService.delete(projectId, sesssion);
//        } catch (IOException e) {
//            e.printStackTrace();
//            return false;
//        }
//
//    }
//
//
//    @Override
//    public boolean projectDeleteAll(@NotNull final Sesssion sesssion) {
//        return projectService.projectDeleteAll(sesssion);
//    }
}
