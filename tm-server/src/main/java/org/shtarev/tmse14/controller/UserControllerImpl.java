package org.shtarev.tmse14.controller;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse14.commands.UserRole;
import org.shtarev.tmse14.entity.Sesssion;
import org.shtarev.tmse14.entity.User;
import org.shtarev.tmse14.service.UserService;

import javax.jws.WebService;
import java.io.IOException;
import java.util.*;

@WebService(endpointInterface = "org.shtarev.tmse14.controller.UserController")
public class UserControllerImpl implements UserController {

    @NotNull
    private final UserService<User> userService;

    public UserControllerImpl(@NotNull final UserService<User> userService) {
        this.userService = userService;
    }

    @Override
    public boolean userCreate(@NotNull final User user) {
        Set<UserRole> listUR = new HashSet<>(Collections.singleton(UserRole.REGULAR_USER));
        user.setUserRole(listUR);
        return userService.create(user);
    }

    @Override
    public List<User> getUserList(@NotNull final Sesssion sesssion) throws Exception {
        Optional<List<User>> listUser = userService.getUserList(sesssion);
        if (listUser.isPresent())
            return listUser.get();
        else throw new Exception();
    }

    @Override
    public Sesssion userLogIn(@NotNull final String name, @NotNull final String password) throws IOException {
        return userService.LogIn(name, password);
    }

    @Override
    public boolean userRePassword(@NotNull final String name, @NotNull final String oldPassword,
                                  @NotNull final String newPassword, @NotNull final Sesssion sesssion) {
        try {
            return userService.rePassword(name, oldPassword, newPassword, sesssion);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    @Override
    public boolean userUpdate(@NotNull final String name, @NotNull final Sesssion sesssion) {
        try {
            return userService.userUpdate(name, sesssion);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public List<UserRole> getUserRole(@NotNull final Sesssion sesssion) throws Exception {
        Optional<List<UserRole>> listUserRole = userService.getListUserRole(sesssion);
        if (listUserRole.isPresent())
            return listUserRole.get();
        else throw new IOException();
    }
}
