package org.shtarev.tmse14.controller;

import org.shtarev.tmse14.commands.UserRole;
import org.shtarev.tmse14.entity.Sesssion;
import org.shtarev.tmse14.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface UserController {

    @WebMethod
    boolean userCreate(User user);

    @WebMethod
    Sesssion userLogIn(String name, String password) throws Exception;

    @WebMethod
    List<User> getUserList(Sesssion sesssion) throws Exception;

    @WebMethod
    List<UserRole> getUserRole(Sesssion sesssion) throws Exception;

    @WebMethod
    boolean userRePassword(String name, String oldPassword, String newPassword, Sesssion sesssion);

    @WebMethod
    boolean userUpdate(String name, Sesssion sesssion);
}
