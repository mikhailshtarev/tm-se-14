package org.shtarev.tmse14.controller;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse14.entity.Sesssion;
import org.shtarev.tmse14.entity.Task;
import org.shtarev.tmse14.service.TaskService;

import javax.jws.WebService;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@WebService(endpointInterface = "org.shtarev.tmse14.controller.TaskController")
public class TaskControllerImpl implements TaskController {
//
    @NotNull
    private final TaskService<Task> taskService;

    public TaskControllerImpl(@NotNull final TaskService<Task> taskService) {
        this.taskService = taskService;
    }
//
//    @Override
//    public boolean taskCreate(@NotNull final Task task, @NotNull final Sesssion sesssion) {
//        return taskService.create(task, sesssion);
//    }
//
//    @Override
//    public List<Task> taskReadAll(@NotNull final Sesssion sesssion) throws IOException {
//        Optional<List<Task>> listTask = taskService.taskReadAll(sesssion);
//        if (listTask.isPresent())
//            return listTask.get();
//        else throw new IOException();
//    }
//
//    @Override
//    public List<Task> taskFindByDescription(@NotNull final String partName, @NotNull final Sesssion sesssion) throws IOException {
//        Optional<List<Task>> listTask = taskService.findByDescription(partName, sesssion);
//        if (listTask.isPresent())
//            return listTask.get();
//        else throw new IOException();
//    }
//
//    @Override
//    public boolean taskUpdate(@NotNull final String taskId, @NotNull final Task task,
//                              @NotNull final String taskPS, @NotNull final Sesssion sesssion) {
//        return taskService.update(taskId, task, taskPS, sesssion);
//    }
//
//    @Override
//    public boolean taskDelete(@NotNull final String id, @NotNull final Sesssion sesssion) {
//        return taskService.delete(id, sesssion);
//    }
//
//
//    @Override
//    public boolean taskDeleteAll(@NotNull final Sesssion sesssion) {
//        return taskService.taskDeleteAll(sesssion);
//    }
}
