package org.shtarev.tmse14;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse14.commands.UserRole;
import org.shtarev.tmse14.controller.UserControllerImpl;
import org.shtarev.tmse14.entity.Project;
import org.shtarev.tmse14.entity.Task;
import org.shtarev.tmse14.entity.User;
import org.shtarev.tmse14.repository.*;
import org.shtarev.tmse14.service.*;
import org.shtarev.tmse14.util.HibernateUtil;

import javax.persistence.EntityManagerFactory;
import javax.xml.ws.Endpoint;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class AppPTManager {

    private EntityManagerFactory emf = HibernateUtil.getEntityManagerFactory();
    @NotNull
    private final UserRepository<User> userRepository = new UserRepositoryImpl(emf);
    @NotNull
    private final SessionRepository sessionRepository = new SessionRepositoryImpl(emf);
    @NotNull
    private final TaskRepository<Task> taskRepository = new TaskRepositoryImpl(emf);
    @NotNull
    private final ProjectRepository<Project> projectRepository = new ProjectRepositoryImpl(emf);
    @NotNull
    private final SessionUtil sessionUtil = new SessionUtilImpl(sessionRepository);
    @NotNull
    private final ProjectService<Project> projectService = new ProjectServiceImpl(projectRepository, taskRepository, userRepository, sessionUtil);
    @NotNull
    private final UserService<User> userService = new UserServiceImpl(userRepository, sessionUtil);
    @NotNull
    private final TaskService<Task> taskService = new TaskServiceImpl(taskRepository, userRepository, sessionUtil);

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final AppPTManager appPTManager = new AppPTManager();
        appPTManager.createTwoUser();
        System.setProperty("javax.xml.transform.TransformerFactory", "com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl");
//        Endpoint.publish("http://0.0.0.0:8080/ProjectEndopoint", new ProjectControllerImpl(appPTManager.projectService));
//        Endpoint.publish("http://0.0.0.0:8080/TaskEndopoint", new TaskControllerImpl(appPTManager.taskService));
        Endpoint.publish("http://0.0.0.0:8080/UserEndopoint", new UserControllerImpl(appPTManager.userService));
    }

    public void createTwoUser() {
        @NotNull final  Set<UserRole> listUR = new HashSet<>(Collections.singleton(UserRole.REGULAR_USER));
        @NotNull final  Set<UserRole> listUR2 = new HashSet<>(Collections.singleton(UserRole.ADMIN));
        User user1 = new User();
        User user2 = new User();
        user1.setName("adminn");
        user1.setPassword("adminn");
        user1.setUserRole(listUR2);
        user2.setName("regular");
        user2.setPassword("regular");
        user2.setUserRole(listUR);
        userService.create(user1);
        userService.create(user2);
    }
}
