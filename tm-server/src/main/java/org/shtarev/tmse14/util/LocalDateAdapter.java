package org.shtarev.tmse14.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * An adapter for the bean properties of {@link LocalDate} type.
 */
public class LocalDateAdapter extends XmlAdapter<String, LocalDateTime>
{
    /**
     * Converts {@link LocalDateTime} into a string value.
     * @param value a value to convert. Can be null.
     * @return a string value.
     */
    @Override
    public String marshal(LocalDateTime value)
            throws Exception
    {
        return value == null ? null : value.toString();
    }

    /**
     * Converts a string value into a {@link LocalDate}
     * instance.
     * @param value a value to convert. Can be null.
     * @return a {@link LocalDate} instance.
     */
    @Override
    public LocalDateTime unmarshal(String value)
            throws Exception
    {
        if (value == null)
        {
            return null;
        }

        int p = value.indexOf('T');

        if (p < 0)
        {
            return LocalDateTime.parse(value);
        }

        while(++p < value.length())
        {
            switch(value.charAt(p))
            {
                case '+':
                case '-':
                case 'Z':
                {
                    return ZonedDateTime.parse(value).toLocalDateTime();
                }
            }

        }

        return LocalDateTime.parse(value);
    }
}

