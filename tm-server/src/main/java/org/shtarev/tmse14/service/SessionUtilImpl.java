package org.shtarev.tmse14.service;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse14.entity.Sesssion;
import org.shtarev.tmse14.entity.User;
import org.shtarev.tmse14.repository.SessionRepository;
import org.shtarev.tmse14.util.HashProperties;
import org.shtarev.tmse14.util.SignatureUtil;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class SessionUtilImpl implements SessionUtil {

    private final SessionRepository sessionRepository;
    ScheduledExecutorService ses = Executors.newScheduledThreadPool(1);
    Runnable sesDel = this::deleteSession;
    ScheduledFuture<?> scheduledFuture = ses.scheduleAtFixedRate(sesDel, 5, 3600, TimeUnit.SECONDS);

    public SessionUtilImpl(@NotNull final SessionRepository sessionRepository) {
        this.sessionRepository = sessionRepository;
    }

    public Optional<Sesssion> createSession(@NotNull final User thisUser) {
        Sesssion sesssion = new Sesssion();
        sesssion.setDataCreate(LocalDateTime.now());
        sesssion.setUserName(thisUser.getName());
        sesssion.setHashBox(SignatureUtil.sign(sesssion, HashProperties.getSalt(), HashProperties.getCycol()));
        if (sessionRepository.createSes(sesssion)) {
            return Optional.of(sesssion);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public boolean changeUserName(@NotNull final String newName, @NotNull final Sesssion sesssion) {
        sesssion.setUserName(newName);
        return sessionRepository.update(sesssion);
    }

    @Override
    public void deleteSession() {
        try {
            Optional<List<Sesssion>> listSessions = sessionRepository.getListSessions();
            listSessions.ifPresent(sesssions -> sesssions.forEach(sesssion -> {
                if (sesssion.getDataCreate().plusDays(1).getDayOfYear() < LocalDate.now().getDayOfYear()) {
                    sessionRepository.deleteSession(sesssion);
                }
            }));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Optional<Sesssion> checkSessionByName(@NotNull final User user) throws Exception {
        try {
            return sessionRepository.getSession(user.getName());
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception();
        }
    }

    public boolean checkSession(@NotNull final Sesssion sesssion) throws Exception {
        try {
            Optional<Sesssion> session1 = sessionRepository.getSession(sesssion.getUserName());
            return session1.get().getHashBox().equals(SignatureUtil.sign(sesssion, HashProperties.getSalt(), HashProperties.getCycol()));
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
