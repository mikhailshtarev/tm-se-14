package org.shtarev.tmse14.service;

import org.shtarev.tmse14.commands.UserRole;
import org.shtarev.tmse14.entity.Sesssion;
import org.shtarev.tmse14.entity.User;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface UserService<U extends User> {

    boolean create(U user);

    Optional<List<U>> getUserList(Sesssion sesssion) throws Exception ;

    Sesssion LogIn(String name, String password) throws IOException;

    boolean rePassword(String name, String oldPassword, String newPassword, Sesssion sesssion) throws Exception;

    boolean userUpdate(String name, Sesssion sesssion)throws Exception;

    Optional<List<UserRole>> getListUserRole(Sesssion sesssion) throws Exception;
}