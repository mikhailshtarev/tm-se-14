package org.shtarev.tmse14.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.shtarev.tmse14.commands.UserRole;
import org.shtarev.tmse14.entity.Sesssion;
import org.shtarev.tmse14.entity.User;
import org.shtarev.tmse14.repository.UserRepository;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import static org.shtarev.tmse14.commands.UserRole.ADMIN;
import static org.shtarev.tmse14.commands.UserRole.REGULAR_USER;

@Slf4j
public class UserServiceImpl implements UserService<User> {

    @NotNull
    private final UserRepository<User> userRepository;

    @NotNull
    private final SessionUtil sessionUtil;

    @NotNull
    public UserServiceImpl(@NotNull final UserRepository<User> userRepository, @NotNull final SessionUtil sessionUtil) {
        this.userRepository = userRepository;
        this.sessionUtil = sessionUtil;
    }

    @Override
    public boolean create(@NotNull final User user) {
        if ("".equals(user.getName()) || "".equals(user.getPassword()) || user.getName() == null
                || user.getUserRole() == null || user.getPassword() == null
                || user.getName().toCharArray().length < 6 || user.getPassword().toCharArray().length < 6) {
            return false;
        }
        user.setPassword(DigestUtils.md5Hex(user.getPassword()));
        try {
            return userRepository.create(user);
        } catch (SQLException throwable) {
            throwable.printStackTrace();
            return false;
        }
    }

    @Override
    public Sesssion LogIn(@NotNull final String name, @NotNull final String password) throws IOException {
        if ("".equals(name) || "".equals(password) || name.toCharArray().length < 6 || password.toCharArray().length < 6) {
            throw new IOException();
        }
        try {
            Optional<User> userOpt = userRepository.getUser(name);
            if (userOpt.isPresent()) {
                @NotNull final User thisUser = userOpt.get();
                @NotNull final String thisPassword = DigestUtils.md5Hex(password);
                if (name.equals(thisUser.getName()) && thisPassword.equals(thisUser.getPassword())) {
                    Optional<Sesssion> sessionOpt = sessionUtil.checkSessionByName(thisUser);
                        if (sessionOpt.isPresent()){
                            return sessionOpt.get();
                        }
                        else {
                            return sessionUtil.createSession(thisUser).get();
                        }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        throw new IOException();
    }

    @Nullable
    public Optional<List<User>> getUserList(@NotNull final Sesssion sesssion) throws Exception {
        if (sessionUtil.checkSession(sesssion)) {
            User user;
            try {
                Optional<User> userOpt = userRepository.getUser(sesssion.getUserName());
                if (userOpt.isPresent())
                    user = userOpt.get();
                else return Optional.empty();
                if (user.getUserRole() != null && (user.getUserRole()).contains(ADMIN)
                ) {
                    return userRepository.getUserList();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        throw new IOException();
    }


    @Override
    public boolean rePassword(@NotNull String name, @NotNull String oldPassword, @NotNull String newPassword,
                              @NotNull final Sesssion sesssion) throws Exception {
        if (sessionUtil.checkSession(sesssion)) {
            if (name.isEmpty() || oldPassword.isEmpty() || newPassword.isEmpty()) {
                return false;
            }
            @NotNull final String oldPassword2 = DigestUtils.md5Hex(oldPassword);
            try {
                @NotNull User user;
                Optional<User> userOpt = userRepository.getUser(sesssion.getUserName());
                if (userOpt.isPresent())
                    user = userOpt.get();
                else return false;
                if (user.getUserRole() != null && (user.getUserRole().contains(ADMIN) || user.getUserRole().contains(REGULAR_USER))) {
                    if (user.getPassword().equals(oldPassword2)) {
                        @NotNull final String newPassword2 = DigestUtils.md5Hex(newPassword);
                        user.setPassword(newPassword2);
                        return userRepository.updateUser(user);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }


    @Override
    public boolean userUpdate(@NotNull final String newName, @NotNull final Sesssion sesssion) throws Exception {
        if (sessionUtil.checkSession(sesssion)) {
            if (newName.isEmpty() || sesssion.getUserName().isEmpty()) {
                return false;
            }
            if (newName.toCharArray().length < 6)
                return false;
            try {
                User user;
                Optional<User> userOpt = userRepository.getUser(sesssion.getUserName());
                if (userOpt.isPresent())
                    user = userOpt.get();
                else return false;
                if (user.getUserRole() != null && (user.getUserRole().contains(ADMIN) || user.getUserRole().contains(REGULAR_USER))) {
                    user.setName(newName);
                    boolean bool = userRepository.updateUser(user);
                    if (bool) {
                        sessionUtil.changeUserName(newName, sesssion);
                        return true;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }


    @Override
    public Optional<List<UserRole>> getListUserRole(Sesssion sesssion) throws Exception {
        if (sessionUtil.checkSession(sesssion)) {
            return userRepository.getListUserRole(sesssion.getUserName());
        }
        return  Optional.empty();
    }
}


