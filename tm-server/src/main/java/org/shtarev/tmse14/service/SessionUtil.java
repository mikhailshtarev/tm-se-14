package org.shtarev.tmse14.service;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse14.entity.Sesssion;
import org.shtarev.tmse14.entity.User;

import java.util.Optional;

public interface SessionUtil {

    Optional<Sesssion> createSession(@NotNull User user);

    boolean checkSession(@NotNull Sesssion sesssion) throws Exception;

    boolean changeUserName(String newName, Sesssion sesssion);

    void deleteSession();

    Optional<Sesssion> checkSessionByName(User user) throws Exception;
}
