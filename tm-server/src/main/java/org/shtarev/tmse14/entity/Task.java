package org.shtarev.tmse14.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.shtarev.tmse14.commands.TaskProjectStatus;
import org.shtarev.tmse14.util.LocalDateAdapter;

import javax.persistence.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "tasks")
public class Task {

    @Id
    @NotNull
    @Column(name = "task_id", nullable = false)
    private String id = UUID.randomUUID().toString();

    @Column(name = "task_name", unique = false, nullable = false)
    private String name;

    @Column(name = "task_description", unique = false, nullable = false)
    private String description;

    @Column(name = "task_date_start", unique = false, nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "uuuu-MM-dd")
    private LocalDate dataStart;

    @Column(name = "task_date_finish", unique = false, nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "uuuu-MM-dd")
    private LocalDate dataFinish;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "project_id", referencedColumnName = "project_id")
    private Project project;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    private User user;

    @Column(name = "task_project_status", unique = false, nullable = false)
    @Type( type = "org.shtarev.tmse14.commands.TaskProjectStatus" )
    private Set<TaskProjectStatus> taskProjectStatus;

    @Column(name = "task_date_create", unique = false, nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "uuuu-MM-dd")
    private LocalDate dataCreate = LocalDate.now();

}