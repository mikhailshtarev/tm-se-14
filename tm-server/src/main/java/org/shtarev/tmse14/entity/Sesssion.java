package org.shtarev.tmse14.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.shtarev.tmse14.util.LocalDateAdapter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDateTime;
import java.util.UUID;


@XmlRootElement(name = "session")
@Getter
@Setter
@Entity
@Table(name = "sessions")
public class Sesssion {

    @Id
    @Column(name = "session_id", nullable = false)
    private String id = UUID.randomUUID().toString();


    @Column(name = "session_user_name", unique = true, nullable = false)
    private String userName;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "uuuu-MM-dd")
    @Column(name = "session_data_create", nullable = false)
    private LocalDateTime dataCreate;

    @Column(name = "session_hashbox", nullable = false)
    private String hashBox;

    @XmlJavaTypeAdapter(value = LocalDateAdapter.class)
    public LocalDateTime getDataCreate() {
        return dataCreate;
    }

}
