package org.shtarev.tmse14.entity;

import lombok.Getter;
import lombok.Setter;
import org.shtarev.tmse14.commands.UserRole;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Set;
import java.util.UUID;

@XmlRootElement(name = "user")
@Getter
@Setter
@Entity
@Table(name = "users_table")
public class User {



    @Column(name = "user_name", unique = true, nullable = false)
    private String name;

    @Column(name = "user_password", nullable = false, length = 100)
    private String password;

    @Column(name = "user_role", nullable = false)
    @ElementCollection(targetClass = UserRole.class, fetch = FetchType.EAGER)
    @CollectionTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"))
    @Enumerated(EnumType.STRING)
    private Set<UserRole> userRole;

    @Id
    @Column(name = "user_id", unique = true, nullable = false)
    private String userId = UUID.randomUUID().toString();


}
