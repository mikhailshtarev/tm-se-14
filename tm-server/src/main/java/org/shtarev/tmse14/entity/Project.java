package org.shtarev.tmse14.entity;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse14.commands.TaskProjectStatus;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Set;
import java.util.UUID;


@Getter
@Setter
@Entity
@Table(name = "projects")
public class Project implements Serializable {


    @Id
    @NotNull
    @Column(name = "project_id", nullable = false)
    public String id = UUID.randomUUID().toString();


    @Column(name = "project_name", unique = false, nullable = false)
    private String name;


    @Column(name = "project_description", unique = false, nullable = false)
    private String description;

    @Column(name = "project_date_start", unique = false, nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "uuuu-MM-dd")
    private LocalDate dataStart;

    @Column(name = "project_date_finish", unique = false, nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "uuuu-MM-dd")
    private LocalDate dataFinish;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private User user;

    @Column(name = "task_project_status", unique = false, nullable = false)
    @Type( type = "org.shtarev.tmse14.commands.TaskProjectStatus" )
    private Set<TaskProjectStatus> taskProjectStatus;

    @Column(name = "project_date_create", unique = false, nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "uuuu-MM-dd")
    private LocalDate dataCreate = LocalDate.now();
}
