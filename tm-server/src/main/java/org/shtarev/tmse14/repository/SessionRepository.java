package org.shtarev.tmse14.repository;

import org.shtarev.tmse14.entity.Sesssion;

import java.util.List;
import java.util.Optional;

public interface SessionRepository {

    boolean createSes(Sesssion sesssion);

    Optional<Sesssion> getSession(String sessionId) throws Exception;

    Optional<List<Sesssion>> getListSessions() throws Exception;

    boolean update(Sesssion sesssion);

    boolean deleteSession(Sesssion sesssion);


}
