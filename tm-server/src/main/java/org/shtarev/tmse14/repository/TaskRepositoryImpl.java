package org.shtarev.tmse14.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.shtarev.tmse14.commands.StrInTPStatus;
import org.shtarev.tmse14.entity.Task;

import javax.persistence.EntityManagerFactory;
import java.io.IOException;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class TaskRepositoryImpl implements TaskRepository<Task> {
    private final EntityManagerFactory emf;

    public TaskRepositoryImpl(@NotNull final EntityManagerFactory emf) {
        this.emf = emf;
    }
//
//    private final String TASKS_TABLE = "tasks";
//    private final String TASK_ID = "task_id";
//    private final String TASK_NAME = "task_name";
//    private final String TASK_DESCRIPTION = "task_description";
//    private final String TASK_DATA_START = "data_start";
//    private final String TASK_DATA_FINISH = "data_finish";
//    private final String TASK_T_P_STATUS = "t_p_status";
//    private final String TASK_DATA_CREATE = "data_create";
//    private final String PROJECT_ID = "project_id";
//    private final String USER_ID = "user_id";
//
//    private Connection getDbConnection()
//            throws ClassNotFoundException, SQLException {
//        final String dbPort = "127.0.0.1:3306";
//        final String dbUser = "root";
//        final String dbPass = "foxfox";
//        final String dbName = "projecttaskbd";
//        final String dbTimezone = "useUnicode=true&serverTimezone=UTC&useSSL=true&verifyServerCertificate=false";
//        String connectionString = "jdbc:mysql://" + dbPort + "/" + dbName + "?" + dbTimezone;
//        return DriverManager
//                .getConnection(connectionString, dbUser, dbPass);
//    }
//
//    @SneakyThrows
//    @Override
//    public boolean create(@NotNull final Task thisTask) {
//        String insert = "INSERT IGNORE INTO " + TASKS_TABLE + "(" +
//                TASK_ID + "," + TASK_NAME + ","
//                + TASK_DESCRIPTION + "," + TASK_DATA_START + ","
//                + TASK_DATA_FINISH + "," + PROJECT_ID + ","
//                + USER_ID + "," + TASK_T_P_STATUS + ","
//                + TASK_DATA_CREATE + ")" +
//                "VALUES(?,?,?,?,?,?,?,?,?)";
//        try (PreparedStatement prST = getDbConnection().prepareStatement(insert)) {
//            prST.setString(1, thisTask.getId());
//            prST.setString(2, thisTask.getName());
//            prST.setString(3, thisTask.getDescription());
//            prST.setString(4, String.valueOf(thisTask.getDataStart()));
//            prST.setString(5, String.valueOf(thisTask.getDataFinish()));
//            prST.setString(6, thisTask.getProjectId());
//            prST.setString(7, thisTask.getUserId());
//            prST.setString(8, Arrays.toString(thisTask.getTaskProjectStatus()));
//            prST.setString(9, String.valueOf(thisTask.getDataCreate()));
//            prST.execute();
//            return true;
//        } catch (SQLException e) {
//            e.printStackTrace();
//            return false;
//        }
//    }
//
//    @Override
//    public boolean update(@NotNull final String taskId, @NotNull final Task thisTask) {
//        String select = "UPDATE " + TASKS_TABLE + " SET " + TASK_DATA_START + "='" + thisTask.getDescription()
//                + "'," + TASK_DATA_START + "='" + thisTask + "'," + TASK_DATA_FINISH + "='"
//                + thisTask.getDataFinish() + "'," + TASK_T_P_STATUS + "='"
//                + Arrays.toString(thisTask.getTaskProjectStatus()) + "' WHERE "
//                + TASK_ID + "='" + taskId + "'";
//        try (PreparedStatement prST = getDbConnection().prepareStatement(select)) {
//            prST.executeUpdate();
//            return true;
//        } catch (SQLException | ClassNotFoundException e) {
//            e.printStackTrace();
//        }
//        return false;
//
//    }
//
//
//    @Override
//    @Nullable
//    public Optional<List<Task>> getTasks(String userId) throws IOException {
//        ResultSet resSet;
//        String select = "SELECT * FROM " + TASKS_TABLE + " WHERE " + USER_ID + "='" + userId + "'";
//        try (PreparedStatement prST = getDbConnection().prepareStatement(select)) {
//            List<Task> taskList = new ArrayList<>();
//            resSet = prST.executeQuery();
//            while (resSet.next()) {
//                Task task = new Task();
//                task.setId(resSet.getString(TASK_ID));
//                task.setName(resSet.getString(TASK_NAME));
//                task.setDescription(resSet.getString(TASK_DESCRIPTION));
//                task.setDataStart(LocalDate.parse(resSet.getString(TASK_DATA_START)));
//                task.setDataFinish(LocalDate.parse(resSet.getString(TASK_DATA_FINISH)));
//                task.setProjectId(resSet.getString(PROJECT_ID));
//                task.setUserId(resSet.getString(USER_ID));
//                if (!StrInTPStatus.strInTPStatus(resSet.getString(TASK_T_P_STATUS)).isPresent())
//                    throw new IOException();
//                task.setTaskProjectStatus(StrInTPStatus.strInTPStatus(resSet.getString(TASK_T_P_STATUS)).get());
//                task.setDataCreate(LocalDate.parse(resSet.getString(TASK_DATA_CREATE)));
//                taskList.add(task);
//            }
//            return Optional.of(taskList);
//        } catch (SQLException | ClassNotFoundException e) {
//            e.printStackTrace();
//
//        }
//        return Optional.empty();
//    }
//
//    @Override
//    public boolean remove(@NotNull final String id, @NotNull final String userId) {
//        String insert = "DELETE FROM " + TASKS_TABLE + " WHERE "
//                + TASK_ID + "='" + id + "';";
//        try (PreparedStatement prST = getDbConnection().prepareStatement(insert)) {
//            prST.executeUpdate();
//            return true;
//        } catch (SQLException | ClassNotFoundException e) {
//            e.printStackTrace();
//            return false;
//        }
//
//    }
//
//    @Override
//    public boolean removeAll(@NotNull final String userId) {
//        String insert = "DELETE FROM " + TASKS_TABLE + " WHERE "
//                + USER_ID + "='" + userId + "';";
//        try (PreparedStatement prST = getDbConnection().prepareStatement(insert)) {
//            prST.executeUpdate();
//            return true;
//        } catch (SQLException | ClassNotFoundException e) {
//            e.printStackTrace();
//            return false;
//        }
//    }
}


