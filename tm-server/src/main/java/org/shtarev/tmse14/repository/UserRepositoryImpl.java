package org.shtarev.tmse14.repository;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse14.commands.UserRole;
import org.shtarev.tmse14.entity.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;


public class UserRepositoryImpl implements UserRepository<User> {
    private final EntityManagerFactory emf;

    public UserRepositoryImpl(@NotNull final EntityManagerFactory emf) {
        this.emf = emf;
    }

    @Override
    public boolean create(@NotNull final User thisUser) throws SQLException {
        try {
            EntityManager eM = emf.createEntityManager();
            eM.getTransaction().begin();
            eM.persist(thisUser);
            eM.getTransaction().commit();
            eM.close();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    //todo
    public Optional<User> getUser(@NotNull final String userName) throws Exception  {
        try {
            EntityManager em = emf.createEntityManager();
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<User> cr = cb.createQuery(User.class);
            Root<User> root = cr.from(User.class);
            cr.select(root).where(cb.equal(root.get("name"), userName));
            TypedQuery<User> query = em.createQuery(cr);
            List<User> resultList = query.getResultList();
            User user1 = resultList.get(0);
            return Optional.ofNullable(user1);
        } catch (Exception e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    @Override
    public Optional<List<User>> getUserList() throws Exception {
        try {
            @NotNull final EntityManager eM = emf.createEntityManager();
            CriteriaBuilder cb = eM.getCriteriaBuilder();
            CriteriaQuery<User> cq = cb.createQuery(User.class);
            Root<User> rootEntry = cq.from(User.class);
            CriteriaQuery<User> all = cq.select(rootEntry);
            TypedQuery<User> allQuery = eM.createQuery(all);
            List<User> listUser = allQuery.getResultList();
            return Optional.of(listUser);
        } catch (Exception e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    @Override
    public boolean updateUser(@NotNull final User user) {
        try {
            @NotNull final EntityManager eM = emf.createEntityManager();
            eM.getTransaction().begin();
            eM.merge(user);
            eM.getTransaction().commit();
            eM.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public Optional<List<UserRole>> getListUserRole(@NotNull final String userName) {
        try {
            @NotNull final EntityManager em = emf.createEntityManager();
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<User> cr = cb.createQuery(User.class);
            Root<User> root = cr.from(User.class);
            cr.select(root).where(cb.equal(root.get("name"), userName));
            TypedQuery<User> query = em.createQuery(cr);
            List<User> resultList = query.getResultList();
            User user1 = resultList.get(0);
            List<UserRole> listRole = new ArrayList<>();
            Set<UserRole> setRole = user1.getUserRole();
            setRole.forEach(userRole -> {
                if (userRole.toString().equals("REGULAR_USER"))
                    listRole.add(UserRole.REGULAR_USER);
                if (userRole.toString().equals("ADMIN"))
                    listRole.add(UserRole.ADMIN);
            });
            return Optional.of(listRole);
        } catch (Exception e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }
}
