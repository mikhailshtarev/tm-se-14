package org.shtarev.tmse14.repository;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse14.entity.Sesssion;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

public class SessionRepositoryImpl implements SessionRepository {
    private final EntityManagerFactory emf;

    public SessionRepositoryImpl(@NotNull final EntityManagerFactory emf) {
        this.emf = emf;
    }

    @Override
    public boolean createSes(@NotNull final Sesssion sesssion) {
        try {
            @NotNull final EntityManager eM = emf.createEntityManager();
            eM.getTransaction().begin();
            eM.persist(sesssion);
            eM.getTransaction().commit();
            eM.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public Optional<Sesssion> getSession(@NotNull final String sessionUserName) throws Exception {
        try {
            @NotNull final  EntityManager eM = emf.createEntityManager();
            CriteriaBuilder cb = eM.getCriteriaBuilder();
            CriteriaQuery<Sesssion> cr = cb.createQuery(Sesssion.class);
            Root<Sesssion> root = cr.from(Sesssion.class);
            cr.select(root).where(cb.equal(root.get("userName"), sessionUserName));
            TypedQuery<Sesssion> query = eM.createQuery(cr);
            List<Sesssion> resultList = query.getResultList();
            Sesssion sesssion = resultList.get(0);
            return Optional.of(sesssion);
        } catch (Exception e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    @Override
    public Optional<List<Sesssion>> getListSessions() throws Exception {
        try {
            @NotNull final EntityManager eM = emf.createEntityManager();
            CriteriaBuilder cb = eM.getCriteriaBuilder();
            CriteriaQuery<Sesssion> cq = cb.createQuery(Sesssion.class);
            Root<Sesssion> rootEntry = cq.from(Sesssion.class);
            CriteriaQuery<Sesssion> all = cq.select(rootEntry);
            TypedQuery<Sesssion> allQuery = eM.createQuery(all);
            List<Sesssion> listSessions = allQuery.getResultList();
            return Optional.of(listSessions);
        } catch (Exception e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    @Override
    public boolean update(Sesssion sesssion) {
        try {
            @NotNull final EntityManager eM = emf.createEntityManager();
            eM.getTransaction().begin();
            eM.merge(sesssion);
            eM.getTransaction().commit();
            eM.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean deleteSession(Sesssion sesssion) {
        try {
            @NotNull final EntityManager eM = emf.createEntityManager();
            eM.getTransaction().begin();
            eM.remove(sesssion);
            eM.getTransaction().commit();
            eM.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
