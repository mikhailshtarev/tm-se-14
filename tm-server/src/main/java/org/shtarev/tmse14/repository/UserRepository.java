package org.shtarev.tmse14.repository;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse14.commands.UserRole;
import org.shtarev.tmse14.entity.User;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface UserRepository<U extends User> {

    boolean create(User thisUser) throws SQLException;

    Optional<List<User>> getUserList() throws Exception;

    Optional<User> getUser(String userName) throws Exception;

    boolean updateUser(User user);

    Optional<List<UserRole>> getListUserRole(String userName);


}
