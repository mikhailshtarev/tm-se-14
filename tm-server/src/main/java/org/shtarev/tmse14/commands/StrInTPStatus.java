package org.shtarev.tmse14.commands;

import org.jetbrains.annotations.NotNull;

import java.util.Optional;

public class StrInTPStatus {

    public static Optional<TaskProjectStatus[]> strInTPStatus(@NotNull final String tPStatusString) {

        if (("[PLANNED]").equals(tPStatusString)) {
            return Optional.of(new TaskProjectStatus[]{TaskProjectStatus.PLANNED});
        }
        if (("[IN_THE_PROCESS]").equals(tPStatusString)) {
            return Optional.of(new TaskProjectStatus[]{TaskProjectStatus.IN_THE_PROCESS});
        }
        if (("[READY]").equals(tPStatusString)) {
            return Optional.of(new TaskProjectStatus[]{TaskProjectStatus.READY});
        }
        return Optional.empty();
    }
}
