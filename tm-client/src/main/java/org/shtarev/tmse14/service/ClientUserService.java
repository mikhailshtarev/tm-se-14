package org.shtarev.tmse14.service;


import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse14.controller.Sesssion;
import org.shtarev.tmse14.controller.User;
import org.shtarev.tmse14.controller.UserRole;

import java.util.List;

public interface ClientUserService {


    boolean create(User user);

    List<User> getUserList( Sesssion session) throws Exception;

    List<UserRole> getUserRole(Sesssion session) throws Exception;

    Sesssion userLogIn(String name, String password) throws Exception;

    boolean userRePassword(String name, String oldPassword, String newPassword, Sesssion session);

    boolean userUpdate( String name,  Sesssion session);

}
