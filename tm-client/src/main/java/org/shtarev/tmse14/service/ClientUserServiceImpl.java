package org.shtarev.tmse14.service;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse14.controller.*;

import java.lang.Exception;
import java.util.List;

public class ClientUserServiceImpl implements ClientUserService {

    protected UserControllerImplService userControllerImplService = new UserControllerImplService();

    public UserController userControllerImplPort = userControllerImplService.getUserControllerImplPort();


    @Override
    public boolean create(@NotNull final User user) {
        return userControllerImplPort.userCreate(user);
    }

    @Override
    public List<User> getUserList(@NotNull final Sesssion session) throws Exception {
        return userControllerImplPort.getUserList(session);
    }

    @Override
    public Sesssion userLogIn(@NotNull final String name, @NotNull final String password) throws Exception {
        return userControllerImplPort.userLogIn(name, password);
    }

    @Override
    public boolean userRePassword(@NotNull final String name, String oldPassword, @NotNull final String newPassword,
                                  @NotNull final Sesssion session) {
        return userControllerImplPort.userRePassword(name, oldPassword, newPassword, session);
    }

    @Override
    public boolean userUpdate(@NotNull final String name, @NotNull final Sesssion session) {
        return userControllerImplPort.userUpdate(name, session);
    }

    @Override
    public List<UserRole> getUserRole(@NotNull final Sesssion session) throws Exception {
        return userControllerImplPort.getUserRole(session);
    }

}
