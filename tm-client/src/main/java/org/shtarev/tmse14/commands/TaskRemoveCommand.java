//package org.shtarev.tmse14.commands;
//
//import org.jetbrains.annotations.NotNull;
//
//final public class TaskRemoveCommand extends AbstractCommand{
//    @Override
//    @NotNull
//    public String getName() {
//        return "TaskRemove";
//    }
//
//    @Override
//    @NotNull
//    public String getDescription() {
//        return "Remove selected Task";
//    }
//
//    @Override
//    public void execute() {
//        System.out.println("Введите ID задачи которую хотите удалить:");
//        @NotNull final String taskId = serviceLocator.getTerminalService().nextLine();
//        serviceLocator.getClientTaskService().taskDeleteOne(taskId,serviceLocator.getSession());
//    }
//}
