package org.shtarev.tmse14.commands;

import org.shtarev.tmse14.controller.Sesssion;
import org.shtarev.tmse14.controller.UserRole;
import org.shtarev.tmse14.service.ClientProjectService;
import org.shtarev.tmse14.service.ClientTaskService;
import org.shtarev.tmse14.service.ClientUserService;

import java.util.List;
import java.util.Scanner;

public interface ServiceLocator {

    ClientProjectService getClientProjectService();

    ClientTaskService getClientTaskService();

    ClientUserService getClientUserService();

    List<AbstractCommand> getCommands();

    Sesssion getSession();

    void setSession(Sesssion session);

    Scanner getTerminalService();

    void setUserRole(List<UserRole> userRole);

    List<UserRole>getUserRole();
}
