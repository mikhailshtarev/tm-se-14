package org.shtarev.tmse14.commands;

import org.jetbrains.annotations.NotNull;

final public class UserRePassword extends AbstractCommand {
    @Override
    @NotNull
    public String getName() {
        return "UserRePassword";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Update you password";
    }

    @Override
    public void execute() {
        System.out.println("Введите старый пароль: ");
        @NotNull final String oldPassword = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите новый пароль: ");
        @NotNull final String newPassword = serviceLocator.getTerminalService().nextLine();
        try {
            if (oldPassword.equals("") || newPassword.equals("")) {
                System.out.println("Одно из вводимых полей пустое");
                return;
            }
            if (serviceLocator.getSession().getId().isEmpty()) {
                System.out.println("Сначала войдите в аккаунт");
                return;
            }
            if(serviceLocator.getClientUserService().userRePassword(serviceLocator.getSession().getUserName(), oldPassword,
                    newPassword, serviceLocator.getSession()))
                System.out.println("Пароль изменен!");;
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
}
