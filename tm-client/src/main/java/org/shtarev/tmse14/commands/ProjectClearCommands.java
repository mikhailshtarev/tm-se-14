//package org.shtarev.tmse14.commands;
//
//
//import org.jetbrains.annotations.NotNull;
//
//public final class ProjectClearCommands extends AbstractCommand {
//    @Override
//    @NotNull
//    public String getName() {
//        return "ProjectClear";
//    }
//
//    @Override
//    @NotNull
//    public String getDescription() {
//        return "Delete all Project with her tasks";
//    }
//
//    @Override
//    public void execute() throws Exception {
//        serviceLocator.getClientProjectService().projectClear(serviceLocator.getSession());
//    }
//}
