//package org.shtarev.tmse14.commands;
//
//import org.jetbrains.annotations.NotNull;
//import org.shtarev.tmse14.controller.Project;
//
//import java.util.List;
//
//public final class ProjectListCommand extends AbstractCommand {
//    @Override
//    @NotNull
//    public String getName() {
//        return "ProjectList";
//    }
//
//    @Override
//    @NotNull
//    public String getDescription() {
//        return "Show all projects.";
//    }
//
//    @Override
//    public void execute() {
//        System.out.println("[PROJECT LIST]");
//        @NotNull final List<Project> projectList;
//        try {
//            projectList = serviceLocator.getClientProjectService().projectReadAll(serviceLocator.getSession());
//
//            projectList.forEach(project -> {
//                        System.out.println("Статус:" + project.getTaskProjectStatus().toString() +
//                                " Название проекта: " + project.getName() + "  Описание проекта: " + project.getDescription() + "   ID проекта: " + project.getId());
//                        System.out.println("                ID User: " + project.getUserId() + "   Дата начала проекта: " + project.getDataStart() + "  " +
//                                " Дата окончания проекта: " + project.getDataFinish());
//                        System.out.println("                Дата создания в ТМ:  " + project.getDataCreate());
//                        System.out.println();
//                    }
//            );
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//}
